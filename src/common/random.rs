use rand::Rng;

use std::ops::Range;

pub fn next_random(current: usize, range: Range<usize>) -> usize {
    let mut next = rand::thread_rng().gen_range(range.clone());
    while next == current {
        next = rand::thread_rng().gen_range(range.clone());
    }
    next
}

pub fn next_random_of_vec(current: usize, vec: Vec<usize>) -> usize {
    let index = rand::thread_rng().gen_range(0..vec.len());
    let mut next = vec[index];
    while next == current {
        next = vec[next_random(index, 0..vec.len())];
    }
    next
}

pub fn random(range: Range<usize>) -> usize {
    rand::thread_rng().gen_range(range.clone())
}

