pub fn diff(left: &str, right: &str) -> Vec<usize> {
    let mut diffs: Vec<usize> = vec![];
    for (i_left, c) in left.chars().enumerate() {
        if Some(c) != right.chars().nth(i_left)  {
            diffs.push(i_left);
        }
    }
    diffs
}

#[cfg(test)]
mod tests {
    use super::*;
    mod diff {
        use super::*;
        #[test]
        fn same() {
            let left = "hello world";
            let right = "hello world";
            let diff = diff(left, right);
            assert_eq!(diff, Vec::<usize>::new());
        }
        #[test]
        fn different() {
            let left = "hello world";
            let right = "hallo World";
            let diff = diff(left, right);
            assert_eq!(diff, vec![1, 6]);
        }
    }
}

