use std::ops::Range;

pub fn increment_and_wrap(range: Range<usize>, mut value: usize) -> usize {
    let last = range.end.saturating_sub(1);
    if value < last {
        value += 1;
        value
    } else { range.start }
}

pub fn decrement_and_wrap(range: Range<usize>, mut value: usize) -> usize {
    let last = range.end.saturating_sub(1);
    if value > range.start {
        value -= 1;
        value
    } else { last }
}

pub fn increment_up_to(max: usize, mut value: usize) -> usize {
    if value < max {
        value += 1;
    }
    value
}

pub fn decrement_up_to(min: usize, mut value: usize) -> usize {
    if value > min {
        value -= 1;
    }
    value
}

