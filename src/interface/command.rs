use ratatui::text::{Line, Span};
use ratatui::style::{Style, Color, Modifier};

pub fn command<'a>(key: &'a str, descriptor: &'a str) -> Line<'a> {
    let key_highlight = Style::default()
        .add_modifier(Modifier::BOLD)
        .bg(Color::Gray)
        .fg(Color::Black);
    Line::from(vec![
        Span::styled(key, key_highlight),
        Span::styled(" ", Style::default()),
        Span::styled(descriptor, Style::default()),
    ])
}

