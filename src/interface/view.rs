#[derive(PartialEq)]
pub enum View {
    Card,
    CardAnswer,
    CardAnswerInput,
    CardAnswerResult,
    Sort,
}

