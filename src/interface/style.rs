use ratatui::style::Style;
use ratatui::style::Color;

pub fn black_on_white() -> Style {
    Style::default().fg(Color::Black).bg(Color::White)
}

pub fn white_on_black() -> Style {
    Style::default().fg(Color::White).bg(Color::Black)
}

