use anyhow::Result;
use ratatui::prelude::{CrosstermBackend, Terminal};
use crossterm::execute;
use crossterm::terminal;

use std::io;

use crate::app::session::Session;

use super::*;
use super::view::View;

pub struct Tui {
    pub focus: View,
}

impl Tui {
    pub fn new() -> Self {
        Tui {
            focus: View::Card,
        }
    }

    pub fn draw(&self, session: &Session,
                terminal: &mut Terminal<CrosstermBackend<std::io::Stderr>>,
                ) -> Result<()> {
        terminal.draw(|frame| {
            let card_text = session.text();
            match self.focus {
                View::Card => {
                    card_view::render_card_view(card_text, frame);
                },
                View::Sort => {
                    card_view::render_card_view(card_text, frame);
                    sort_modal::render_sort_modal(session.sort(), frame);
                },
                View::CardAnswer => {
                    card_view::render_card_answer_view(card_text, frame);
                },
                View::CardAnswerInput => {
                    let input = session.input();
                    card_view::render_card_answer_input_view(card_text, &input,
                                                             frame);
                },
                View::CardAnswerResult => {
                    let input = session.input();
                    let answer_outcome = session.answer_outcome();
                    let answer_outcome = match answer_outcome {
                        Some(outcome) => outcome.to_string(),
                        None => String::new(),
                    };
                    card_view::render_card_answer_result_view(card_text,
                                                              &answer_outcome,
                                                              &input,
                                                              frame);
                },
            }
        })?;
        Ok(())
    }

    pub fn set_focus(&mut self, view: View) {
        self.focus = view;
    }
}

pub fn enter_tui() -> Result<()> {
    terminal::enable_raw_mode()?;
    execute!(io::stderr(), terminal::EnterAlternateScreen)?;
    Ok(())
}

pub fn exit_tui() -> Result<()> {
    execute!(io::stderr(), terminal::LeaveAlternateScreen)?;
    terminal::disable_raw_mode()?;
    Ok(())
}
