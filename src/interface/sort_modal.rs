use ratatui::{prelude::*, widgets::*};
use ratatui::layout::Rect;
use strum::IntoEnumIterator;

use std::rc::Rc;

use crate::app::sort::Sort;
use crate::common::count;

use super::command;
use super::style;

pub fn render_sort_modal(active: &Sort, frame: &mut Frame) {
    let width = frame.size().width;
    let height = frame.size().height;
    let mut modal = modal_inner(modal_outer_relative(35, 13, frame), false);
    let mut list = modal_list(modal_list_items_all(active));
    if width < 58 || height < 24 {
        modal = modal_inner(modal_outer_absolute(20, 5, frame), true);
        list = modal_list(modal_list_items_3(active));
    }
    let content_container = modal[0];
    let cmd_palette_container = modal[1];
    let cmd_palette = cmd_palette(cmd_palette_container.into());
    for (index, column) in commands().iter().enumerate() {
        frame.render_widget(column.clone(), cmd_palette[index]);
    }
    Clear.render(content_container, frame.buffer_mut());
    frame.render_widget(list, content_container);
}

fn modal_outer_relative(width: u16, height: u16, frame: &mut Frame) -> Rect {
    let layout_y = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
                     Constraint::Length((frame.size().height - height) / 2),
                     Constraint::Length(height),
                     Constraint::Length((frame.size().height - height) / 2),
        ]);
    let layout_x = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
                     Constraint::Percentage((100 - width) / 2),
                     Constraint::Percentage(width),
                     Constraint::Percentage((100 - width) / 2),
        ]);
    layout_x.split(layout_y.split(frame.size())[1])[1]
}

fn modal_outer_absolute(width: u16, height: u16, frame: &mut Frame) -> Rect {
    let layout_y = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
                     Constraint::Length((frame.size().height - height) / 2),
                     Constraint::Length(height),
                     Constraint::Length((frame.size().height - height) / 2),
        ]);
    let layout_x = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
                     Constraint::Length((frame.size().width - width) / 2),
                     Constraint::Length(width),
                     Constraint::Length((frame.size().width - width) / 2),
        ]);
    layout_x.split(layout_y.split(frame.size())[1])[1]
}

fn modal_inner(outer: Rect, minimize: bool) -> Rc<[Rect]> {
    if minimize {
        Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                         Constraint::Percentage(100),
                         Constraint::Max(0),
            ])
            .split(outer)
    } else {
        Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                         Constraint::Percentage(100),
                         Constraint::Min(1),
            ])
            .split(outer)
    }
}

fn modal_list(items: Vec<ListItem>) -> List {
    let list_wrapper = Block::default()
        .borders(Borders::ALL);
    List::new(items).block(list_wrapper).style(style::white_on_black())
}

fn modal_list_items_all(active: &Sort) -> Vec<ListItem> {
    let mut items = vec![];
    for sort_enum in Sort::iter() {
        if sort_enum == *active {
            let item = ListItem::new(sort_enum.to_string())
                .style(style::black_on_white());
            items.push(item);
        } else {
            let item = ListItem::new(sort_enum.to_string())
                .style(style::white_on_black());
            items.push(item);
        }
    }
    items
}

fn modal_list_items_3(active: &Sort) -> Vec<ListItem> {
    let mut items = vec![];
    let sort: Vec<Sort> = Sort::iter().collect();
    for (i_sort, sort_enum) in Sort::iter().enumerate() {
        if sort_enum == *active {
            let i_prev = count::decrement_and_wrap(0..sort.len(), i_sort);
            if let Some(item_before) = sort.get(i_prev) {
                let item_before = ListItem::new(item_before.to_string())
                    .style(style::white_on_black());
                items.push(item_before);
            }
            let item_active = ListItem::new(sort_enum.to_string())
                .style(style::black_on_white());
            items.push(item_active);
            let i_next = count::increment_and_wrap(0..sort.len(), i_sort);
            if let Some(item_after) = sort.get(i_next) {
                let item_after = ListItem::new(item_after.to_string())
                    .style(style::white_on_black());
                items.push(item_after);
            }
        }
    }
    items
}

fn cmd_palette(outer: Rc<Rect>) -> Rc<[Rect]> {
    let padding_left = 1;
    let padded_palette = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(padding_left),
            Constraint::Length(outer.width - padding_left),
        ])
        .split(*outer);
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Ratio(1, 3),
            Constraint::Ratio(1, 3),
            Constraint::Ratio(1, 3),
        ])
        .split(padded_palette[1])
}

fn commands() -> Vec<Paragraph<'static>> {
    let cmd_col_1 = Paragraph::new(vec![command::command("j", "down")]);
    let cmd_col_2 = Paragraph::new(vec![command::command("k", "up")]);
    let cmd_col_3 = Paragraph::new(vec![command::command("s", "close")]);
    vec![cmd_col_1, cmd_col_2, cmd_col_3]
}

