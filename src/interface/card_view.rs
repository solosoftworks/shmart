use ratatui::Frame;
use ratatui::prelude::Rect;
use ratatui::widgets::{Paragraph, Block, Borders, Wrap, Padding};
use ratatui::layout::{Constraint, Alignment, Layout, Direction};
use ratatui::text::{Line, Span};
use ratatui::style::{Style, Color};

use std::rc::Rc;

use crate::common::text;

use super::command;

pub fn render_card_view(text: &str, frame: &mut Frame) {
    let layout_main = layout_main(frame.size().height, frame.size().width)
        .split(frame.size());
    let card_container = layout_main[0];
    let card = card(
        Line::from(text),
        card_padding(text, card_container, 3_f32));
    let command_palette_contanier = layout_main[1];
    let command_palette = command_palette(command_palette_contanier.into());
    for (index, column) in commands().iter().enumerate() {
        frame.render_widget(column.clone(), command_palette[index]);
    }
    frame.render_widget(card, card_container);
}

pub fn render_card_answer_view(text: &str, frame: &mut Frame) {
    let layout_main = layout_main(frame.size().height, frame.size().width)
        .split(frame.size());
    let card_container = layout_main[0];
    let card = card(
        Line::from(text),
        card_padding(text, card_container, 3_f32));
    let answer_prompt_contanier = layout_main[1];
    let answer_prompt = answer_prompt(answer_prompt_contanier.into());
    let col_prompt = Paragraph::new(vec![
        Line::from("remembered? "),
        Line::from(""),
    ]).alignment(Alignment::Right);
    let col_answers = Paragraph::new(vec![
        command::command("y", "yes"),
        command::command("n", "no"),
    ]);
    frame.render_widget(col_prompt.clone(), answer_prompt[0]);
    frame.render_widget(col_answers.clone(), answer_prompt[1]);
    frame.render_widget(card, card_container);
}

pub fn render_card_answer_input_view(text: &str, input: &str,
                                     frame: &mut Frame) {
    let layout_main = layout_main(frame.size().height, frame.size().width)
        .split(frame.size());
    let card_container = layout_main[0];
    let card = card(
        Line::from(text),
        card_padding(text, card_container, 3_f32));
    let answer_prompt_contanier = layout_main[1];
    let answer_prompt = answer_exact_prompt(answer_prompt_contanier.into());
    let col_prompt = Paragraph::new(vec![
        command::command("enter", "input answer:"),
        Line::from(format!("{input}|")),
    ]).alignment(Alignment::Left);
    frame.render_widget(col_prompt.clone(), answer_prompt[1]);
    frame.render_widget(card, card_container);
}

pub fn render_card_answer_result_view(text: &str, answer_outcome: &str,
                                      input: &str, frame: &mut Frame) {
    let layout_main = layout_main(frame.size().height, frame.size().width)
        .split(frame.size());
    let card_container = layout_main[0];
    let card = card(
        line_highlight(text, input),
        card_padding(text, card_container, 3_f32));
    let answer_prompt_contanier = layout_main[1];
    let answer_prompt = answer_exact_prompt(answer_prompt_contanier.into());
    let col_prompt = Paragraph::new(vec![
        Line::from(format!{"{answer_outcome} answer ({input})"}),
        command::command("enter", "confirm"),
    ]).alignment(Alignment::Left);
    frame.render_widget(col_prompt.clone(), answer_prompt[1]);
    frame.render_widget(card, card_container);
}

fn layout_main(height: u16, width: u16) -> Layout {
    if width < 58 || height < 24 {
        Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                 Constraint::Percentage(100),
                 Constraint::Max(0),
            ])
    } else {
        Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                 Constraint::Percentage(100),
                 Constraint::Min(2),
            ])
    }
}

fn card(text: Line, padding: (u16, u16, u16, u16)) -> Paragraph {
    Paragraph::new(text)
            .wrap(Wrap { trim: true })
            .alignment(Alignment::Center)
            .block(Block::default()
                .borders(Borders::ALL)
                .padding(Padding::new(padding.0, padding.1, padding.2,
                                      padding.3))
            )
}

fn card_padding(text: &str, container: Rect,
                padding_horizontal: f32) -> (u16, u16, u16, u16) {
    let frame_height = container.height as f32;
    let center_absolut = (frame_height / 2.0).floor();
    let text_width = text.chars().count() as f32;
    let frame_width = container.width as f32;
    let lines_of_text =
        ((text_width - padding_horizontal * 2.0) / frame_width).ceil();
    let bias = 1.0;
    let left = 3;
    let right = 3;
    let top = center_absolut - (lines_of_text / 2.0).floor() - bias;
    let bottom = 0;
    (left, right, top as u16, bottom)
}

fn line_highlight<'a>(text: &'a str, input: &str) -> Line<'a> {
    let diff = text::diff(text, input);
    let incorrect = Style::default().fg(Color::White).bg(Color::Red);
    let correct = Style::default().fg(Color::White).bg(Color::Green);
    let mut spans: Vec<Span> = vec![];
    for (i, c) in text.chars().enumerate() {
        if diff.contains(&i) {
            spans.push(Span::styled(c.to_string(), incorrect));
        } else {
            spans.push(Span::styled(c.to_string(), correct));
        }
    }
    Line::from(spans)
}

fn answer_prompt(outer: Rc<Rect>) -> Rc<[Rect]> {
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Ratio(1, 2),
            Constraint::Ratio(1, 2),
        ])
        .split(*outer)
}

fn answer_exact_prompt(outer: Rc<Rect>) -> Rc<[Rect]> {
    let padding_left = 1;
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(padding_left),
            Constraint::Length(outer.width - padding_left),
        ])
        .split(*outer)
}

fn command_palette(outer: Rc<Rect>) -> Rc<[Rect]> {
    let padding_left = 1;
    let padded_palette = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Length(padding_left),
            Constraint::Length(outer.width - padding_left),
        ])
        .split(*outer);
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Ratio(1, 5),
            Constraint::Ratio(1, 5),
            Constraint::Ratio(1, 5),
            Constraint::Ratio(1, 5),
            Constraint::Ratio(1, 5),
        ])
        .split(padded_palette[1])
}

fn commands() -> Vec<Paragraph<'static>> {
    let cmd_col_1 = Paragraph::new(vec![
        command::command("n", "next"),
        command::command("p", "previous"),
    ]);
    let cmd_col_2 = Paragraph::new(vec![
        command::command("f", "flip"),
        command::command(" ", "continue"),
    ]);
    let cmd_col_3 = Paragraph::new(vec![
        command::command("a", "answer"),
        command::command("A", "exact"),
    ]);
    let cmd_col_4 = Paragraph::new(vec![
        command::command("m", "mode"),
        command::command("s", "sort"),
    ]);
    let cmd_col_5 = Paragraph::new(vec![
        command::command("q", "quit"),
    ]);
    vec![
        cmd_col_1,
        cmd_col_2,
        cmd_col_3,
        cmd_col_4,
        cmd_col_5,
    ]
}

