use thiserror::Error;

use std::io::Error as IoError;
use std::env::VarError;
use std::path::PathBuf;

#[derive(Debug, Error)]
pub enum ErrorAnswers {
    #[error("error: environment variable '$HOME' not found, expected path to \
            users home directory")]
    HomeNotFound(VarError),
    #[error("error: unable to create '{1}', expected WRITE permissions")]
    PermissionDenied(IoError, PathBuf),
    #[error("error: unable to read or write data correctly, consider deleting \
            '{1}'")]
    GeneralFailure(IoError, PathBuf),
}

