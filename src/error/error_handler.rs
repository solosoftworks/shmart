use anyhow::Error;

use std::process::exit;
use std::backtrace::Backtrace;

use crate::interface::tui;
use crate::constants::project::{ERROR_TICKET_URL, ERROR_TICKET_EMAIL};
use crate::constants::exit_codes::exit_code;

use super::error_toml::ErrorToml;
use super::error_answers::ErrorAnswers;

pub fn resolve(error: Error) {
    tui::exit_tui().expect("expected to exit tui");
    if let Some(ErrorToml::FileNotFound(_, _))
        | Some(ErrorToml::BadFormat(_, _))
        | Some(ErrorToml::PermissionDenied(_, _))
        = error.downcast_ref::<ErrorToml>() {
        print_toml_error(error);
        exit(exit_code::INVALID_ARGS);
    }
    if let Some(ErrorAnswers::HomeNotFound(_))
        | Some(ErrorAnswers::PermissionDenied(_, _))
        | Some(ErrorAnswers::GeneralFailure(_, _))
        = error.downcast_ref::<ErrorAnswers>() {
        print_answers_error(error);
        exit(exit_code::INVALID_ARGS);
    }
    print_unexpected_error(error);
    exit(exit_code::FAILURE);
}

pub fn print_toml_error(error: Error) {
    eprintln!();
    eprintln!("program exited early due to bad input:");
    eprintln!();
    eprintln!("{error}");
    eprintln!();
    eprintln!("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    eprintln!("hint: you can fix the problem by analyzing the message above");
    eprintln!();
    eprintln!("for more information, try '--help' section [common errors]");
}

pub fn print_answers_error(error: Error) {
    eprintln!();
    eprintln!("program exited early because of a problem with your system:");
    eprintln!();
    eprintln!("{error}");
    eprintln!();
    eprintln!("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    eprintln!("hint: you may be able to fix the problem by analyzing the message above");
    eprintln!("hint: you may be able to avoid errors of this kind by using '--isolate' flag");
    eprintln!();
    eprintln!("for more information, try '--help' section [common errors]");
}

pub fn print_unexpected_error(error: Error) {
    eprintln!();
    eprintln!("ERROR_MESSAGE; start");
    eprintln!("###################################################");
    let backtrace = Backtrace::force_capture();
    eprintln!("{:#?}", backtrace);
    eprintln!("{:#?}", error);
    eprintln!("###################################################");
    eprintln!("ERROR_MESSAGE; end");
    eprintln!();
    eprintln!("Sorry. An unexpected error occured!");
    eprintln!();
    eprintln!("Please, help us fix the problem by");
    eprintln!("1) copy error message above");
    let fmtstr= format!("2) email to '{ERROR_TICKET_EMAIL}'");
    eprintln!("{fmtstr}");
    eprintln!("- or -");
    let fmtstr = format!("1) visit '{ERROR_TICKET_URL}'");
    eprintln!("{fmtstr}");
    eprintln!("2) create ticket");
}

