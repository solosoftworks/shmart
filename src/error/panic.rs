use std::panic;
use std::backtrace::Backtrace;

use crate::constants::project::{ERROR_TICKET_URL, ERROR_TICKET_EMAIL};
use crate::interface::tui;

pub fn with_context() {
    panic::set_hook(Box::new(|panic_info| {
        tui::exit_tui().expect("expected to exit tui");
        eprintln!();
        eprintln!("PANIC_MESSAGE; start");
        eprintln!("###################################################");
        let backtrace = Backtrace::force_capture();
        eprintln!("{:#?}", backtrace);
        eprintln!("{:#?}", panic_info);
        eprintln!("###################################################");
        eprintln!("PANIC_MESSAGE; end");
        eprintln!();
        eprintln!("!!! PROGRAM PANIC !!!");
        eprintln!("the program terminated appruptly due to an irrecoverable \
                  error");
        eprintln!("integrity of user data cannot be guaranteed");
        eprintln!();
        eprintln!("please, help us fix this high priority error by");
        eprintln!("1) copy panic message above");
        let fmtstr= format!("2) email to '{ERROR_TICKET_EMAIL}'");
        eprintln!("{fmtstr}");
        eprintln!("- or -");
        let fmtstr = format!("1) visit '{ERROR_TICKET_URL}'");
        eprintln!("{fmtstr}");
        eprintln!("2) create ticket");
    }));
}

