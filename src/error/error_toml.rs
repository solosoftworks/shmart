use thiserror::Error;
use toml::de as toml;

use std::io;
use std::path::PathBuf;

#[derive(Error, Debug)]
pub enum ErrorToml {
    #[error("{0}\nerror: bad format in file '{1}', expected valid TOML \
            format")]
    BadFormat(toml::Error, PathBuf),
    #[error("error: file '{1}' not found, expected valid file path")]
    FileNotFound(io::Error, PathBuf),
    #[error("error: insufficient permissions for file '{1}', expected READ \
            and WRITE permissions")]
    PermissionDenied(io::Error, PathBuf),
}

