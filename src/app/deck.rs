use crate::data::answer::Answer;
use crate::data::card::Card;

use super::playing_card::PlayingCard;

pub fn build_deck(data: Vec<&Card>, answers: Vec<Answer>) -> Vec<PlayingCard> {
    let mut deck = vec![];
    for card in data.iter() {
        let card_id = format!("{}{}", card.front, card.back);
        let mut history = vec![];
        for card_answers in answers.iter() {
            if card_answers.card_id == card_id {
                history = card_answers.answers.clone();
            }
        }
        let playing_card = PlayingCard {
            text: card,
            history,
        };
        deck.push(playing_card);
    }
    deck
}

