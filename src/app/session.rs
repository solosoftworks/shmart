use strum::IntoEnumIterator;
use chrono::offset::Local;
use chrono::{ Datelike, Weekday };

use crate::common::count;
use crate::common::random;
use crate::data::answer::Answer;
use crate::data::card::Card;

use super::deck;
use super::mode::Mode;
use super::sort::Sort;
use super::face::Face;
use super::playing_card::PlayingCard;
use super::answer_outcome::AnswerOutcome;

#[derive(PartialEq, Debug, Default)]
pub struct Session<'a> {
    deck: Vec<PlayingCard<'a>>,
    deck_index: usize,
    deck_history: Vec<usize>,
    mode: Mode,
    sort: Sort,
    face: Face,
    should_quit: bool,
    input: Vec<String>,
}

impl<'a> Session<'a> {
    pub fn new(data: Vec<&'a Card>, answers: Vec<Answer>, mode: Mode, sort: Sort) -> Self {
        Session {
            deck: deck::build_deck(data, answers),
            deck_index: 0,
            deck_history: vec![],
            mode,
            sort,
            face: Session::default_face(mode),
            should_quit: false,
            input: vec![],
        }
    }

    pub fn deck(&self) -> Vec<PlayingCard> {
        self.deck.clone()
    }

    pub fn text(&self) -> &str {
        let card_text = self.deck.get(self.deck_index)
            .expect("expected 'deck_index' to be valid")
            .text;
        match self.face {
            Face::Front => &card_text.front,
            Face::Back => &card_text.back,
        }
    }

    pub fn answer_outcome(&self) -> Option<&AnswerOutcome> {
        match self.deck.get(self.deck_index) {
            Some(outcome) => outcome.history.last(),
            None => None,
        }
    }

    pub fn quit(&mut self) {
        self.should_quit = true;
    }

    pub fn should_quit(&self) -> bool {
        self.should_quit
    }

    pub fn next_card(&mut self) {
        self.deck_history.push(self.deck_index);
        self.face = Session::default_face(self.mode);
        match self.sort {
            Sort::Sequential => {
                self.deck_index = self.next_sequential();
            },
            Sort::Shuffle => {
                self.deck_index = self.next_shuffle();
            }
            Sort::Random => {
                let range = 0..self.deck.len();
                self.deck_index = random::next_random(self.deck_index, range);
            },
            Sort::Leitner => {
                self.deck_index = self.next_leitner();
            },
            Sort::WeightedLigthly => {
                self.deck_index = self.next_weighted(5, true);
            }
            Sort::WeightedHeavily => {
                self.deck_index = self.next_weighted(10, false);
            }
            Sort::WorstSeven => {
                self.deck_index = self.next_worst(7);
            },
            Sort::WorstQuarter => {
                self.deck_index = self.next_worst_by_percent(25);
            },
            Sort::Incorrect => {
                self.deck_index = self.next_incorrect();
            }
        }
    }

    pub fn previous_card(&mut self) {
        self.face = Session::default_face(self.mode);
        self.deck_index = self.deck_history.pop().unwrap_or(0);
    }

    pub fn flip_card(&mut self) {
        match self.face {
            Face::Front => self.face = Face::Back,
            Face::Back => self.face = Face::Front,
        }
    }

    pub fn next_side(&mut self) {
        match self.mode {
            Mode::Front => {
                match self.face {
                    Face::Front => self.flip_card(),
                    Face::Back => self.next_card(),
                }
            },
            Mode::Back => {
                match self.face {
                    Face::Front => self.next_card(),
                    Face::Back => self.flip_card(),
                }
            },
        }
    }

    pub fn sort(&self) -> &Sort {
        &self.sort
    }

    pub fn cycle_sort(&mut self, clockwise: bool) {
        self.deck_history.clear();
        if clockwise {
            self.sort = Sort::iter()
                .skip_while(|x| *x != self.sort)
                .nth(1)
                .unwrap_or(Sort::iter().next()
                           .expect("expected 'Sort' to be implemented"));
        } else {
            self.sort = Sort::iter()
                .rev()
                .skip_while(|x| *x != self.sort)
                .nth(1)
                .unwrap_or(Sort::iter().next_back()
                           .expect("expected 'Sort' to be implemented"));
        }
    }

    pub fn cycle_mode(&mut self) {
        self.mode = match self.mode {
            Mode::Front => Mode::Back,
            Mode::Back => Mode::Front,
        };
    }

    pub fn input(&self) -> String {
        self.input.concat()
    }

    pub fn input_push(&mut self, key: char) {
        self.input.push(key.to_string());
    }

    pub fn input_pop(&mut self) {
        self.input.pop();
    }

    pub fn input_clear(&mut self) {
        self.input = vec![];
    }

    pub fn answer_card(&mut self, outcome: AnswerOutcome) {
        if let Some(card) = self.deck.get_mut(self.deck_index) {
            card.add_answer(outcome);
        }
    }

    pub fn answer_card_exact(&mut self, answer: &str) {
        if let Some(card) = self.deck.get_mut(self.deck_index) {
            let correct = match self.face {
                Face::Front => card.text.back.clone(),
                Face::Back => card.text.front.clone(),
            };
            if answer == correct {
                card.add_answer(AnswerOutcome::Correct);
            } else {
                card.add_answer(AnswerOutcome::Incorrect);
            }
        }
    }

    fn next_sequential(&mut self) -> usize {
        count::increment_and_wrap(0..self.deck.len(), self.deck_index)
    }

    fn next_shuffle(&mut self) -> usize {
        if self.deck_history.len() < self.deck.len() {
            self.next_sequential()
        } else {
            let mut previous_deck = self.extract_previous_deck_from_history();
            let current_deck = self.extract_current_deck_from_history();
            previous_deck.retain(|&x| !current_deck.contains(&x));
            previous_deck[random::random(0..previous_deck.len())]
        }
    }

    fn next_weighted(&self, max_factor: usize, is_liear: bool) -> usize {
        let mut weighted_indices: Vec<usize> = vec![];
        for (i_deck, card) in self.deck.iter().enumerate() {
            let weight = Self::weigh_card(&card.history, max_factor, is_liear);
            for _ in 0..weight {
                weighted_indices.push(i_deck);
            }
        }
        random::next_random_of_vec(self.deck_index, weighted_indices)
    }

    fn weigh_card(history: &[AnswerOutcome], max_factor: usize,
                  is_liear: bool) -> usize {
        let mut weight: usize = 1;
        for outcome in history.iter() {
            match outcome {
                AnswerOutcome::Incorrect => {
                    if is_liear {
                        weight = count::increment_up_to(max_factor, weight);
                    } else {
                        weight = max_factor;
                    }
                },
                AnswerOutcome::Correct => {
                    weight = count::decrement_up_to(1, weight);
                },
            };
        }
        weight
    }

    fn next_leitner(&self) -> usize {
        let mut box_index: Vec<(usize, usize)> = vec![];
        for (i_deck, card) in self.deck.iter().enumerate() {
            let leitner_box = Self::assign_box(&card.history);
            box_index.push((leitner_box, i_deck));
        }
        let cards = Self::todays_leitner_cards(box_index,
                                               Local::now().weekday());
        if cards.is_empty() {
            self.deck_index
        } else {
            let i_cards = self.deck_history.len() % cards.len();
            cards[i_cards]
        }
    }

    fn todays_leitner_cards(box_index: Vec<(usize, usize)>,
                            day: Weekday) -> Vec<usize> {
        let cards: Vec<usize> = match day {
            Weekday::Mon => box_index.into_iter()
                .filter(|x| x.0 == 1)
                .map(|x| x.1)
                .collect(),
            Weekday::Tue => box_index.into_iter()
                .filter(|x| x.0 == 1 || x.0 == 2)
                .map(|x| x.1)
                .collect(),
            Weekday::Wed => box_index.into_iter()
                .filter(|x| x.0 == 1 || x.0 == 3)
                .map(|x| x.1)
                .collect(),
            Weekday::Thu => box_index.into_iter()
                .filter(|x| x.0 == 1 || x.0 == 2)
                .map(|x| x.1)
                .collect(),
            Weekday::Fri => box_index.into_iter()
                .filter(|x| x.0 == 1 || x.0 == 4)
                .map(|x| x.1)
                .collect(),
            Weekday::Sat => box_index.into_iter()
                .filter(|x| x.0 == 1 || x.0 == 2 || x.0 == 3)
                .map(|x| x.1)
                .collect(),
            Weekday::Sun => box_index.into_iter()
                .filter(|x| x.0 == 1 || x.0 == 5)
                .map(|x| x.1)
                .collect(),
        };
        cards
    }

    fn assign_box(history: &[AnswerOutcome]) -> usize {
        let mut leitner_box = 1;
        for outcome in history.iter().rev() {
            if leitner_box == 5 ||
                *outcome == AnswerOutcome::Incorrect {
                break;
            }
            if *outcome == AnswerOutcome::Correct {
                leitner_box += 1;
            }
        }
        leitner_box
    }

    fn next_worst_by_percent(&self, limit_percent: usize) -> usize {
        let limit = self.deck.len() as f32 / 100.0 * limit_percent as f32;
        let limit = limit.round() as usize;
        self.next_worst(limit)
    }

    fn next_worst(&self, mut limit: usize) -> usize {
        if self.deck.len() < limit { limit = self.deck.len() }
        let i_bottom = self.deck_history.len() % limit;
        let mut index_count: Vec<(usize, usize)> = vec![];
        for (i_deck, card) in self.deck.iter().enumerate() {
            index_count.push((
                    i_deck,
                    card.count_answers_of_type(AnswerOutcome::Incorrect),
            ));
        }
        index_count.sort_by_key(|&(_, amount)| amount);
        index_count.reverse();
        let next_tuple = index_count.get(i_bottom)
            .expect("expected 'limit' to be smaller or equal to 'deck.len()'");
        next_tuple.0
    }

    fn next_incorrect(&self) -> usize {
        let incorrect_cards = self.extract_incorrect_cards_from_deck();
        if let Some(i_current_card) = incorrect_cards.iter()
            .position(|&x| x == self.deck_index) {
            let mut i_next_card = i_current_card + 1;
            if i_next_card >= incorrect_cards.len() {
                i_next_card = 0;
            }
            *incorrect_cards.get(i_next_card).expect("expected 'i_next_card' to be in range")
        } else if let Some(first) = incorrect_cards.first() {
            *first
        } else {
            self.deck_index
        }
    }

    fn extract_incorrect_cards_from_deck(&self) -> Vec<usize> {
        let mut incorrect_cards = vec![];
        for (i_deck, card) in self.deck.iter().enumerate() {
            if let Some(answer) = card.history.last() {
                if *answer == AnswerOutcome::Incorrect {
                    incorrect_cards.push(i_deck);
                }
            }
        }
        incorrect_cards
    }

    fn extract_current_deck_from_history(&self) -> Vec<usize> {
        let current_length = self.deck_history.len() % self.deck.len();
        let current_start =
            self.deck_history.len() - current_length;
        let current_end =
            self.deck_history.len();
        self.deck_history[current_start..current_end].to_vec()
    }

    fn extract_previous_deck_from_history(&self) -> Vec<usize> {
        let current_length = self.deck_history.len() % self.deck.len();
        let previous_start =
            self.deck_history.len() - current_length - self.deck.len();
        let previous_end =
            self.deck_history.len() - current_length;
        self.deck_history[previous_start..previous_end].to_vec()
    }

    fn default_face(mode: Mode) -> Face {
        match mode {
            Mode::Back => Face::Back,
            Mode::Front => Face::Front,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::card::Card;

    fn mock_two_cards() -> Vec<Card> {
        vec![
            Card { front: "1f".to_string(), back: "1b".to_string() },
            Card { front: "2f".to_string(), back: "2b".to_string() },
        ]
    }

    fn mock_three_cards() -> Vec<Card> {
        vec![
            Card { front: "1f".to_string(), back: "1b".to_string() },
            Card { front: "2f".to_string(), back: "2b".to_string() },
            Card { front: "3f".to_string(), back: "3b".to_string() },
        ]
    }

    fn mock_four_cards() -> Vec<Card> {
        vec![
            Card { front: "1f".to_string(), back: "1b".to_string() },
            Card { front: "2f".to_string(), back: "2b".to_string() },
            Card { front: "3f".to_string(), back: "3b".to_string() },
            Card { front: "4f".to_string(), back: "4b".to_string() },
        ]
    }

    fn mock_base_deck(data: Vec<&Card>) -> Vec<PlayingCard> {
        let mut deck = vec![];
        for card_text in data.iter() {
            deck.push(PlayingCard::new(card_text));
        }
        deck
    }

    mod answer_outcome {
        use super::*;
        #[test]
        fn some() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                ..Default::default()
            };
            let card = session.deck.get_mut(session.deck_index).unwrap();
            card.add_answer(AnswerOutcome::Incorrect);
            card.add_answer(AnswerOutcome::Correct);
            let outcome = session.answer_outcome();
            assert_eq!(outcome, Some(&AnswerOutcome::Correct));
        }
        #[test]
        fn none() {
            let data = mock_two_cards();
            let session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                ..Default::default()
            };
            let outcome = session.answer_outcome();
            assert_eq!(outcome, None);
        }
    }

    mod answer_card {
        use super::*;
        #[test]
        fn correct() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                ..Default::default()
            };
            session.answer_card(AnswerOutcome::Correct);
            let card = session.deck.get(session.deck_index).unwrap();
            let last_answer = card.history.last().unwrap();
            assert_eq!(last_answer, &AnswerOutcome::Correct);
        }
        #[test]
        fn incorrect() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                ..Default::default()
            };
            session.answer_card(AnswerOutcome::Incorrect);
            let card = session.deck.get(session.deck_index).unwrap();
            let last_answer = card.history.last().unwrap();
            assert_eq!(last_answer, &AnswerOutcome::Incorrect);
        }
    }

    mod answer_card_exact {
        use super::*;
        #[test]
        fn correct() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                face: Face::Front,
                ..Default::default()
            };
            session.answer_card_exact("1b");
            let card = session.deck.get(session.deck_index).unwrap();
            let last_answer = card.history.last().unwrap();
            assert_eq!(last_answer, &AnswerOutcome::Correct);
        }
        #[test]
        fn incorrect() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                face: Face::Front,
                ..Default::default()
            };
            session.answer_card_exact("WRONG_ANSWER");
            let card = session.deck.get(session.deck_index).unwrap();
            let last_answer = card.history.last().unwrap();
            assert_eq!(last_answer, &AnswerOutcome::Incorrect);
        }
    }

    mod flip_card {
        use super::*;
        #[test]
        fn back_to_front() {
            let mut session = Session {
                face: Face::Back,
                ..Default::default()
            };
            session.flip_card();
            assert_eq!(session.face, Face::Front);
        }
        #[test]
        fn front_to_back() {
            let mut session = Session {
                face: Face::Front,
                ..Default::default()
            };
            session.flip_card();
            assert_eq!(session.face, Face::Back);
        }
    }

    mod next_card {
        use super::*;
        #[test]
        fn increment() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                ..Default::default()
            };
            session.next_card();
            assert_eq!(session.deck_index, 1);
        }
        #[test]
        fn increment_max() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 1,
                ..Default::default()
            };
            session.next_card();
            assert_eq!(session.deck_index, 0);
        }
        #[test]
        fn increment_empty() {
            let mut session = Session {
                deck: vec![],
                deck_index: 0,
                ..Default::default()
            };
            session.next_card();
            assert_eq!(session.deck_index, 0);
        }
        #[test]
        fn push_to_history() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                deck_history: vec![],
                ..Default::default()
            };
            session.next_card();
            assert_eq!(session.deck_history, vec![0]);
        }
        #[test]
        fn flip_to_front() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                face: Face::Back,
                ..Default::default()
            };
            session.next_card();
            assert_eq!(session.face, Face::Front);
        }
    }

    mod next_leitner {
        use super::*;
        #[test]
        fn valid() {
            let data = mock_two_cards();
            let session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                deck_history: vec![0],
                ..Default::default()
            };
            assert_eq!(session.next_leitner(), 1);
        }
        #[test]
        fn out_of_cards() {
            let session = Session {
                deck: vec![],
                deck_index: 3,
                ..Default::default()
            };
            assert_eq!(session.next_leitner(), 3);
        }
    }

    mod next_incorrect {
        use super::*;
        fn mock_deck_mixed(data: Vec<&Card>) -> Vec<PlayingCard> {
            let mut deck = vec![];
            let mut pc = PlayingCard::new(data[0]);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Correct);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[1]);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Correct);
            pc.add_answer(AnswerOutcome::Incorrect);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[2]);
            pc.add_answer(AnswerOutcome::Correct);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[3]);
            pc.add_answer(AnswerOutcome::Correct);
            pc.add_answer(AnswerOutcome::Correct);
            pc.add_answer(AnswerOutcome::Incorrect);
            deck.push(pc);
            deck
        }
        fn mock_deck_correct(data: Vec<&Card>) -> Vec<PlayingCard> {
            let mut deck = vec![];
            let mut pc = PlayingCard::new(data[0]);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[1]);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[2]);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[3]);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            deck
        }
        #[test]
        fn first() {
            let data = mock_four_cards();
            let session = Session {
                deck: mock_deck_mixed(data.iter().collect()),
                deck_index: 2,
                deck_history: vec![],
                ..Default::default()
            };
            assert_eq!(session.next_incorrect(), 1);
        }
        #[test]
        fn increment() {
            let data = mock_four_cards();
            let session = Session {
                deck: mock_deck_mixed(data.iter().collect()),
                deck_index: 1,
                deck_history: vec![],
                ..Default::default()
            };
            assert_eq!(session.next_incorrect(), 3);
        }
        #[test]
        fn increment_max() {
            let data = mock_four_cards();
            let session = Session {
                deck: mock_deck_mixed(data.iter().collect()),
                deck_index: 3,
                deck_history: vec![],
                ..Default::default()
            };
            assert_eq!(session.next_incorrect(), 1);
        }
        #[test]
        fn none() {
            let data = mock_four_cards();
            let session = Session {
                deck: mock_deck_correct(data.iter().collect()),
                deck_index: 2,
                deck_history: vec![],
                ..Default::default()
            };
            assert_eq!(session.next_incorrect(), 2);
        }
    }

    mod previous_card {
        use super::*;
        #[test]
        fn pop() {
            let mut session = Session {
                deck_history: vec![0],
                deck_index: 1,
                ..Default::default()
            };
            session.previous_card();
            assert_eq!(session.deck_index, 0);
        }
        #[test]
        fn pop_empty_history() {
            let mut session = Session {
                deck_history: vec![],
                deck_index: 0,
                ..Default::default()
            };
            session.previous_card();
            assert_eq!(session.deck_index, 0);
        }
        #[test]
        fn flip_to_front() {
            let mut session = Session {
                mode: Mode::Back,
                face: Face::Front,
                ..Default::default()
            };
            session.previous_card();
            assert_eq!(session.face, Face::Back);
        }
    }

    mod next_side {
        use super::*;
        #[test]
        fn front_to_back() {
            let mut session = Session {
                face: Face::Front,
                ..Default::default()
            };
            session.next_side();
            assert_eq!(session.face, Face::Back);
        }
        #[test]
        fn back_to_front() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                face: Face::Back,
                ..Default::default()
            };
            session.next_side();
            assert_eq!(session.face, Face::Front);
        }
        #[test]
        fn back_to_next() {
            let data = mock_two_cards();
            let mut session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                face: Face::Back,
                ..Default::default()
            };
            session.next_side();
            assert_eq!(session.deck_index, 1);
        }
    }

    mod next_worst {
        use super::*;
        fn mock_this_deck(data: Vec<&Card>) -> Vec<PlayingCard> {
            let mut deck = vec![];
            let mut pc = PlayingCard::new(data[0]);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Correct);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[1]);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Incorrect);
            deck.push(pc);
            let mut pc = PlayingCard::new(data[2]);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Incorrect);
            pc.add_answer(AnswerOutcome::Correct);
            deck.push(pc);
            deck
        }
        #[test]
        fn increment() {
            let data = mock_three_cards();
            let session = Session {
                deck: mock_this_deck(data.iter().collect()),
                deck_index: 0,
                deck_history: vec![],
                ..Default::default()
            };
            assert_eq!(session.next_worst(2), 1);
        }
        #[test]
        fn increment_max() {
            let data = mock_three_cards();
            let session = Session {
                deck: mock_this_deck(data.iter().collect()),
                deck_index: 0,
                deck_history: vec![1, 2],
                ..Default::default()
            };
            assert_eq!(session.next_worst(2), 1);
        }
        #[test]
        fn increment_out_of_bounds() {
            let data = mock_three_cards();
            let session = Session {
                deck: mock_this_deck(data.iter().collect()),
                deck_index: 0,
                deck_history: vec![1, 2, 0],
                ..Default::default()
            };
            assert_eq!(session.next_worst(4), 1);
        }
        #[test]
        fn order() {
            let data = mock_three_cards();
            let mut session = Session {
                deck: mock_this_deck(data.iter().collect()),
                deck_index: 0,
                deck_history: vec![],
                ..Default::default()
            };
            assert_eq!(session.next_worst(2), 1);
            session.deck_history = vec![1];
            assert_eq!(session.next_worst(2), 2);
            session.deck_history = vec![1, 2];
            assert_eq!(session.next_worst(2), 1);
        }
    }

    mod text {
        use super::*;
        #[test]
        fn front() {
            let data = mock_two_cards();
            let session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 0,
                face: Face::Front,
                ..Default::default()
            };
            assert_eq!(session.text(), "1f");
        }
        #[test]
        fn back() {
            let data = mock_two_cards();
            let session = Session {
                deck: mock_base_deck(data.iter().collect()),
                deck_index: 1,
                face: Face::Back,
                ..Default::default()
            };
            assert_eq!(session.text(), "2b");
        }
    }

    mod cycle_mode {
        use super::*;
        #[test]
        fn front_to_back() {
            let mut session = Session {
                mode: Mode::Front,
                ..Default::default()
            };
            session.cycle_mode();
            assert_eq!(session.mode, Mode::Back);

        }
        #[test]
        fn back_to_front() {
            let mut session = Session {
                mode: Mode::Back,
                ..Default::default()
            };
            session.cycle_mode();
            assert_eq!(session.mode, Mode::Front);
        }
    }

    mod cycle_sort {
        use super::*;
        #[test]
        fn clockwise() {
            let mut session = Session {
                sort: Sort::Sequential,
                ..Default::default()
            };
            session.cycle_sort(true);
            assert_eq!(session.sort, Sort::Shuffle);
        }
        #[test]
        fn counterclockwise() {
            let mut session = Session {
                sort: Sort::WorstSeven,
                ..Default::default()
            };
            session.cycle_sort(false);
            assert_eq!(session.sort, Sort::WeightedHeavily);
        }
        #[test]
        fn first_to_last() {
            let mut session = Session {
                sort: Sort::Sequential,
                ..Default::default()
            };
            session.cycle_sort(false);
            assert_eq!(session.sort, Sort::Incorrect);
        }
        #[test]
        fn last_to_first() {
            let mut session = Session {
                sort: Sort::Incorrect,
                ..Default::default()
            };
            session.cycle_sort(true);
            assert_eq!(session.sort, Sort::Sequential);
        }
    }
}

