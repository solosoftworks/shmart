use std::path::PathBuf;

pub struct AppOptions {
    pub isolate: bool,
    pub path_answers: PathBuf,
}

