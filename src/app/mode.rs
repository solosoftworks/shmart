use clap::ValueEnum;

#[derive(Copy, Clone, Default, Debug, PartialEq, Eq, PartialOrd, Ord,
         ValueEnum)]
pub enum Mode {
    #[default]
    Front,
    Back,
}

