use serde::{Serialize, Deserialize};

use std::fmt;

#[derive(PartialEq, Debug, Clone, Copy, Serialize, Deserialize)]
pub enum AnswerOutcome {
    Correct,
    Incorrect,
}

impl fmt::Display for AnswerOutcome {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            AnswerOutcome::Correct => write!(f, "correct"),
            AnswerOutcome::Incorrect => write!(f, "incorrect"),
        }
    }
}

