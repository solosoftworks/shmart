use anyhow::Result;
use crossterm::event::{self, KeyModifiers};
use crossterm::event::Event::Key;
use crossterm::event::KeyEvent;
use crossterm::event::KeyCode::{Char, Esc, Enter, Backspace};
use ratatui::prelude::{CrosstermBackend, Terminal};

use crate::user_config::cli_options::CliOptions;
use crate::data::card::Card;
use crate::data::cards;
use crate::interface::tui::Tui;
use crate::interface::tui;
use crate::interface::view::View;
use crate::error::error_handler;
use crate::error::panic;
use crate::data::answers;

use super::session::Session;
use super::mode::Mode;
use super::sort::Sort;
use super::answer_outcome::AnswerOutcome;
use super::app_options::AppOptions;

pub fn run() {
    panic::with_context();
    if let Err(error) = start_app() {
        error_handler::resolve(error);
    };
}

fn start_app() -> Result<()> {
    let cli_options = CliOptions::new();
    let isolate = cli_options.isolate;
    let app = AppOptions { isolate, path_answers: answers::path()? };
    let set_paths = cli_options.set;
    let wipe_all = cli_options.wipe_all;
    let wipe_set = cli_options.wipe;
    let data = cards::parse(set_paths)?;
    let data: Vec<&Card> = data.cards.iter().collect();
    let mode = cli_options.mode.unwrap_or(Mode::Front);
    let sort = cli_options.sort.unwrap_or(Sort::Sequential);
    if wipe_all {
        answers::wipe_file(&app.path_answers)?;
    } else if wipe_set {
        answers::create_file(&app.path_answers)?;
        let answers = answers::read_from_file(&app.path_answers)?;
        let set = answers::from_card_data(data.clone(), answers);
        answers::wipe_set(&app.path_answers, set)?;
    }
    let answers = if app.isolate {
        vec![]
    } else {
        answers::create_file(&app.path_answers)?;
        answers::read_from_file(&app.path_answers)?
    };
    let session = Session::new(data, answers, mode, sort);
    let tui = Tui::new();

    start_event_loop(app, tui, session)?;
    Ok(())
}

fn exit_app(app: &AppOptions, session: &mut Session,
            save_on_exit: bool) -> Result<()> {
    if save_on_exit && !app.isolate {
        let answers = answers::from_cards(session.deck());
        let answers = answers::merge_with_existing(&app.path_answers, answers)?;
        answers::write_to_file(&app.path_answers, answers)?;
    }
    session.quit();
    Ok(())
}

fn start_event_loop(app: AppOptions, mut tui: Tui,
                    mut session: Session) -> Result<()> {
    let backend = CrosstermBackend::new(std::io::stderr());
    let mut terminal = Terminal::new(backend)?;
    tui::enter_tui()?;
    while !session.should_quit() {
        tui.draw(&session, &mut terminal)?;
        update_session(&app, &mut tui, &mut session)?;
    }
    tui::exit_tui()?;
    Ok(())
}

fn update_session(app: &AppOptions, tui: &mut Tui,
                  session: &mut Session) -> Result<()> {
    if event::poll(std::time::Duration::from_millis(250))? {
        if let Key(key_event) = event::read()? {
            match tui.focus {
                View::Card =>
                    run_card_commands(key_event, app, tui, session)?,
                View::CardAnswer =>
                    run_card_answer_commands(key_event, app, tui, session)?,
                View::CardAnswerInput =>
                    run_card_answer_input_commands(key_event, app, tui,
                                                   session)?,
                View::CardAnswerResult =>
                    run_card_answer_result_commands(key_event, app, tui,
                                                    session)?,
                View::Sort =>
                    run_sort_commands(key_event, app, tui, session)?,
            }
        }
    }
    Ok(())
}

fn run_card_commands(key_event: KeyEvent, app: &AppOptions, tui: &mut Tui,
                     session: &mut Session) -> Result<()> {
    match key_event.code {
        Char('n') => session.next_card(),
        Char('p') => session.previous_card(),
        Char(' ') => session.next_side(),
        Char('f') => session.flip_card(),
        Char('m') => session.cycle_mode(),
        Char('s') => tui.set_focus(View::Sort),
        Char('a') => {
            tui.set_focus(View::CardAnswer);
            session.flip_card();
        },
        Char('A') => tui.set_focus(View::CardAnswerInput),
        Char('c') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                exit_app(app, session, false)?;
            }
        },
        Char('q') => exit_app(app, session, true)?,
        Esc => exit_app(app, session, false)?,
        _ => {},
    }
    Ok(())
}

fn run_card_answer_commands(
    key_event: KeyEvent,
    app: &AppOptions,
    tui: &mut Tui,
    session: &mut Session) -> Result<()> {
    match key_event.code {
        Char('y') => {
            tui.set_focus(View::Card);
            session.answer_card(AnswerOutcome::Correct);
            session.next_card();
        },
        Char('n') => {
            tui.set_focus(View::Card);
            session.answer_card(AnswerOutcome::Incorrect);
            session.next_card();
        },
        Char('c') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                exit_app(app, session, false)?;
            }
        },
        Esc => exit_app(app, session, false)?,
        _ => {},
    }
    Ok(())
}

fn run_card_answer_input_commands(
    key_event: KeyEvent,
    app: &AppOptions,
    tui: &mut Tui,
    session: &mut Session) -> Result<()> {
    match key_event.code {
        Enter => {
            tui.set_focus(View::CardAnswerResult);
            session.answer_card_exact(&session.input());
            session.flip_card();
        },
        Backspace => session.input_pop(),
        Esc => exit_app(app, session, false)?,
        Char(c) => {
            if c == 'c' && key_event.modifiers == KeyModifiers::CONTROL {
                exit_app(app, session, false)?;
            } else {
                session.input_push(c);
            }
        },
        _ => {},
    }
    Ok(())
}

fn run_card_answer_result_commands(
    key_event: KeyEvent,
    app: &AppOptions,
    tui: &mut Tui,
    session: &mut Session) -> Result<()> {
    match key_event.code {
        Enter => {
            tui.set_focus(View::Card);
            session.next_card();
            session.input_clear();
        },
        Esc => exit_app(app, session, false)?,
        Char('c') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                exit_app(app, session, false)?;
            }
        },
        _ => {},
    }
    Ok(())
}

fn run_sort_commands(key_event: KeyEvent, app: &AppOptions, tui: &mut Tui,
                     session: &mut Session) -> Result<()> {
    match key_event.code {
        Char('j') => session.cycle_sort(true),
        Char('k') => session.cycle_sort(false),
        Char('s') => tui.set_focus(View::Card),
        Esc => exit_app(app, session, false)?,
        Char('c') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                exit_app(app, session, false)?;
            }
        },
        _ => {},
    }
    Ok(())
}

