use crate::data::card::Card;

use super::answer_outcome::AnswerOutcome;

#[derive(PartialEq, Debug, Clone)]
pub struct PlayingCard<'a> {
    pub text: &'a Card,
    pub history: Vec<AnswerOutcome>,
}

impl<'a> PlayingCard<'a> {
    pub fn new(text: &'a Card) -> PlayingCard {
        PlayingCard {
            text,
            history: vec![],
        }
    }

    pub fn add_answer(&mut self, answer: AnswerOutcome) {
        self.history.push(answer);
    }

    pub fn count_answers_of_type(&self, answer: AnswerOutcome) -> usize {
        match answer {
            AnswerOutcome::Correct => self.history.iter()
                .filter(|x| **x == AnswerOutcome::Correct)
                .count(),
            AnswerOutcome::Incorrect => self.history.iter()
                .filter(|x| **x == AnswerOutcome::Incorrect)
                .count(),
        }
    }
}

