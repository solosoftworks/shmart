use clap::ValueEnum;
use strum::{EnumString, EnumIter};

use std::fmt;

#[derive(Clone, PartialEq, Debug, Default, EnumIter, EnumString, ValueEnum)]
pub enum Sort {
    #[default]
    Sequential,
    Shuffle,
    Random,
    Leitner,
    WeightedLigthly,
    WeightedHeavily,
    WorstSeven,
    WorstQuarter,
    Incorrect,
}

impl fmt::Display for Sort {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Sort::Sequential => write!(f, "Sequential"),
            Sort::Shuffle => write!(f, "Shuffle"),
            Sort::Random => write!(f, "Random"),
            Sort::Leitner => write!(f, "Leitner"),
            Sort::WeightedLigthly => write!(f, "Lightly Weighted"),
            Sort::WeightedHeavily => write!(f, "Heavily Weighted"),
            Sort::WorstSeven => write!(f, "Worst Seven"),
            Sort::WorstQuarter => write!(f, "Worst Quarter"),
            Sort::Incorrect => write!(f, "Incorrect Only"),
        }
    }
}

