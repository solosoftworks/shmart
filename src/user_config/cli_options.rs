use std::path::PathBuf;

use clap::Parser;

use crate::app::mode::Mode;
use crate::app::sort::Sort;

#[derive(Parser)]
#[clap(
    author = "Robin Tanch",
    version = "1.2.0",
    about = None,
    long_about = None,
    after_help = after_help())]
pub struct CliOptions {
    /// A TOML formatted set of cards
    #[clap(short, long, required = true, value_parser, value_name = "FILE")]
    pub set: Vec<PathBuf>,
    /// Ignore save file
    #[clap(short, long)]
    pub isolate: bool,
    /// Side of card shown by default
    #[clap(long)]
    pub mode: Option<Mode>,
    /// Order of cards
    #[clap(long)]
    pub sort: Option<Sort>,
    /// Wipe data of any card
    #[clap(short, long)]
    pub wipe_all: bool,
    /// Wipe data of cards in set
    #[clap(long)]
    pub wipe: bool,
}

impl CliOptions {
    pub fn new() -> Self {
        let cli = CliOptions::parse();
        Self {
            set: cli.set,
            isolate: cli.isolate,
            mode: cli.mode,
            sort: cli.sort,
            wipe_all: cli.wipe_all,
            wipe: cli.wipe,
        }
    }
}

fn after_help() -> &'static str {
r#"
<FILE> file.toml
---------------------------
[[cards]]
front = "Card One Front"
back = "Card One Back"

[[cards]]
front = "Card Two Front"
back = "Card Two Back"
---------------------------
For the full manual run 'man shmart'.
Report bugs to rota@solosoftworks.com.
"#
}
