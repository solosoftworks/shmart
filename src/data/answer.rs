use serde::{Serialize, Deserialize};

use crate::app::answer_outcome::AnswerOutcome;

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Answer {
    pub card_id: String,
    pub answers: Vec<AnswerOutcome>,
}

