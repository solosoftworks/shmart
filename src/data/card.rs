use serde::Deserialize;

#[derive(Deserialize, PartialEq, Debug)]
pub struct Card {
    pub front: String,
    pub back: String,
}

