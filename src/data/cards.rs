use serde::Deserialize;
use anyhow::{Result, anyhow};
use toml::de
    as toml;

use std::path::PathBuf;
use std::fs::File;
use std::io::Read;
use std::io::ErrorKind
    as ioKind;

use crate::error::error_toml::ErrorToml;

use super::card::Card;

#[derive(Deserialize, PartialEq, Debug, Default)]
pub struct Cards {
    pub cards: Vec<Card>,
}

pub fn parse(paths: Vec<PathBuf>) -> Result<Cards> {
    let mut cards = Cards::default();
    for path in paths.iter() {
        let file_content = read_file_into_string(path)?;
        let set = toml::from_str(&file_content);
        let mut set: Cards = match set {
            Ok(set) => set,
            Err(error) =>
                return Err(anyhow!(ErrorToml::BadFormat(error, path.to_owned()))),
        };
        cards.cards.append(&mut set.cards);
    }
    Ok(cards)
}

fn read_file_into_string(path: &PathBuf) -> Result<String> {
    let file = File::open(path);
    let mut file = match file {
        Ok(file) => file,
        Err(error) => return Err(open_file_error(error, path.to_owned())),
    };
    let mut file_content = String::new();
    file.read_to_string(&mut file_content)?;
    Ok(file_content)
}

fn open_file_error(error: std::io::Error, path: PathBuf) -> anyhow::Error {
        match error.kind() {
            ioKind::NotFound =>
                anyhow!(ErrorToml::FileNotFound(error, path)),
            ioKind::PermissionDenied =>
                anyhow!(ErrorToml::PermissionDenied(error, path)),
            _ => anyhow!(error),
        }
}

#[cfg(test)]
mod tests {
    use super::*;
    mod parse {
        use std::path::PathBuf;
        use crate::data::card::Card;
        use super::ErrorToml;
        #[test]
        fn valid_file() {
            let paths = vec![PathBuf::from("tests/data/set_basic.toml")];
            let cards = vec![
                Card { front: "1f".to_string(), back: "1b".to_string() },
                Card { front: "2f".to_string(), back: "2b".to_string() },
            ];
            let result = super::parse(paths).unwrap();
            assert_eq!(result.cards, cards);
        }
        #[test]
        fn valid_files() {
            let paths = vec![
                PathBuf::from("tests/data/set_basic.toml"),
                PathBuf::from("tests/data/set_basic.toml"),
            ];
            let cards = vec![
                Card { front: "1f".to_string(), back: "1b".to_string() },
                Card { front: "2f".to_string(), back: "2b".to_string() },
                Card { front: "1f".to_string(), back: "1b".to_string() },
                Card { front: "2f".to_string(), back: "2b".to_string() },
            ];
            let result = super::parse(paths).unwrap();
            assert_eq!(result.cards, cards);
        }
        #[test]
        fn empty_file() {
            let paths = vec![PathBuf::from("tests/data/empty_file.toml")];
            let result = super::parse(paths).unwrap_err();
            let result = result.downcast_ref::<ErrorToml>();
            let is_bad_format =
                matches!(result, Some(ErrorToml::BadFormat(_, _)));
            assert!(is_bad_format);
        }
        #[test]
        fn nonexistent_file() {
            let paths = vec![PathBuf::from("tests/data/nonexistent.toml")];
            let result = super::parse(paths).unwrap_err();
            let result = result.downcast_ref::<ErrorToml>();
            let is_file_not_found =
                matches!(result, Some(ErrorToml::FileNotFound(_, _)));
            assert!(is_file_not_found);
        }
        #[test]
        #[ignore]
        fn no_read_permission() {
            let paths = vec![PathBuf::from("tests/data/set_no_permission_read.toml")];
            let result = super::parse(paths).unwrap_err();
            let result = result.downcast_ref::<ErrorToml>();
            let is_permission_denied =
                matches!(result, Some(ErrorToml::PermissionDenied(_, _)));
            assert!(is_permission_denied);
        }
    }
}

