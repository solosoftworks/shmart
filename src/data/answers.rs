use anyhow::Result;
use anyhow::anyhow;

use std::path::PathBuf;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::ErrorKind as IoKind;
use std::io::Result as IoResult;
use std::env;

use crate::app::playing_card::PlayingCard;
use crate::data::card::Card;
use crate::error::error_answers::ErrorAnswers;
use crate::constants::paths::relative_to_home;

use super::answer::Answer;

pub fn from_card_data(data: Vec<&Card>,
                      all_answers: Vec<Answer>) -> Vec<Answer> {
    let mut answers = vec![];
    for card in data.iter() {
        let card_id = format!("{}{}", card.front, card.back);
        for answer in all_answers.iter() {
            if answer.card_id == card_id {
                answers.push(answer.clone());
            }
        }
    }
    answers
}

pub fn from_cards(cards: Vec<PlayingCard>) -> Vec<Answer> {
    let mut answer_data: Vec<Answer> = vec![];
    for card in cards.iter() {
        let card_id = format!("{}{}", card.text.front, card.text.back);
        let answers = card.history.clone();
        let answer = Answer {
            card_id,
            answers,
        };
        answer_data.push(answer);
    }
    answer_data
}

pub fn path() -> Result<PathBuf> {
    match env::var("HOME") {
        Ok(home) =>
            Ok(PathBuf::from(home).join(relative_to_home::ANSWER_STORE_PATH)),
        Err(error) =>
            Err(anyhow!(ErrorAnswers::HomeNotFound(error))),
    }
}

pub fn merge_with_existing(path: &PathBuf,
                           answers: Vec<Answer>) -> Result<Vec<Answer>> {
    let set = read_from_file(path)?;
    let mut list = vec![];
    for answer in answers.iter() {
        let mut contains_id = false;
        for item in set.iter() {
            if answer.card_id == item.card_id {
                contains_id = true;
            }
        }
        if !contains_id {
            list.push(answer.clone());
        }
    }
    for item in set.iter() {
        list.push(item.clone());
        for answer in answers.iter() {
            if answer.card_id == item.card_id {
                list.pop();
                list.push(answer.clone());
            }
        }
    }
    Ok(list)
}

pub fn write_to_file(path: &PathBuf, answers: Vec<Answer>) -> Result<()> {
    let file = File::create(path)?;
    serde_json::to_writer_pretty(file, &answers)?;
    Ok(())
}

pub fn wipe_file(path: &PathBuf) -> Result<()> {
    File::create(path)?;
    Ok(())
}

pub fn wipe_set(path: &PathBuf, set: Vec<Answer>) -> Result<()> {
    let all = read_from_file(path)?;
    let answers = exclude_set(all, set)?;
    write_to_file(path, answers)?;
    Ok(())
}

pub fn create_file(path: &PathBuf) -> Result<()> {
    let create = create_file_unwrapped(path);
    let path = path.to_path_buf();
    match create {
        Ok(()) => Ok(()),
        Err(error) => {
            let error_answers = match error.kind() {
                IoKind::PermissionDenied => {
                    anyhow!(ErrorAnswers::PermissionDenied(error, path))
                },
                _ => anyhow!(ErrorAnswers::GeneralFailure(error, path)),
            };
            Err(error_answers)
        },
    }
}

pub fn read_from_file(path: &PathBuf) -> Result<Vec<Answer>> {
    let set = read_from_file_unwrapped(path);
    match set {
        Ok(unwrapped_set) => Ok(unwrapped_set),
        Err(error) =>
            Err(anyhow!(ErrorAnswers::GeneralFailure(error,
                                                     path.to_path_buf()))),
    }
}

fn create_file_unwrapped(path: &PathBuf) -> IoResult<()> {
    if let Some(parent) = path.parent() {
        fs::create_dir_all(parent)?;
    }
    if !path.is_file() {
        File::create(path)?;
    }
    Ok(())
}

fn read_from_file_unwrapped(path: &PathBuf) -> IoResult<Vec<Answer>> {
    let mut file = File::open(path)?;
    let mut file_content = String::new();
    file.read_to_string(&mut file_content)?;
    let mut set: Vec<Answer> = vec![];
    if !file_content.is_empty() {
        set = serde_json::from_str(&file_content)?;
    }
    Ok(set)
}

fn exclude_set(all: Vec<Answer>, answers: Vec<Answer>) -> Result<Vec<Answer>> {
    let answers_excluding_set = all.iter()
        .filter(|x| !answers.contains(x))
        .map(|x| x.to_owned())
        .collect();
    Ok(answers_excluding_set)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::app::answer_outcome::AnswerOutcome::{ Correct, Incorrect };

    mod create_file {
        use super::*;
        #[test]
        #[ignore]
        fn no_write_permission() {
            let path = PathBuf::new()
                .join("tests/dir_no_write_permisson")
                .join("local/share/shmart/answer.json");
            let result = create_file(&path).unwrap_err();
            let result = result.downcast_ref::<ErrorAnswers>();
            let is_bad_format =
                matches!(result, Some(ErrorAnswers::PermissionDenied(_, _)));
            assert!(is_bad_format);
        }
    }

    mod exclude_set {
        use super::*;
        #[test]
        fn valid() {
            let all: Vec<Answer> = vec![
                Answer{
                    card_id: "1f1b".to_string(),
                    answers: vec![ Correct, Incorrect ]
                },
                Answer{
                    card_id: "2f2b".to_string(),
                    answers: vec![ Incorrect, Correct ]
                },
            ];
            let set: Vec<Answer> = vec![
                Answer{
                    card_id: "1f1b".to_string(),
                    answers: vec![ Correct, Incorrect ]
                },
            ];
            let expected: Vec<Answer> = vec![
                Answer{
                    card_id: "2f2b".to_string(),
                    answers: vec![ Incorrect, Correct ]
                },
            ];
            let answers = exclude_set(all, set).unwrap();
            assert_eq!(expected, answers);
        }
    }

    mod read_from_file {
        use super::*;
        #[test]
        fn invalid() {
            let path = PathBuf::from("tests/bad_path/answers_partially_empty.json");
            let result = read_from_file(&path).unwrap_err();
            let result = result.downcast_ref::<ErrorAnswers>();
            let is_general_failure =
                matches!(result, Some(ErrorAnswers::GeneralFailure(_, _)));
            assert!(is_general_failure);
        }
    }
}

