use shmart::*;

use app::app_runner;

fn main() {
    app_runner::run();
}

